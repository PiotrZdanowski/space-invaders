# Space Invaders

Space invaders - self evaluate project

GDD
===
Base information
---
2D arcade game with pixel-art style about defending earth from space invaders with use of own spaceship

Made with Unity 3D

Main game states
---
**Loading**

* loading next game state

**Main menu**

* option to start game
* option to quit

**Gameplay**

* player spaceship steering and shooting
* enemies spaceship movement and shooting
* winning or loosing view with exit to main menu

Gameplay
---
**Preset**
* player spaceship is placed on down-middle
* enemies are placed on top-middle in a grid layout

**Gameplay loop**
* enemies move once per set of time in right or left - when they reach one side of screen, they move down and proceed to move in the other direction
* enemies shoot bullets once per 5 seconds from a random enemy spaceship
* player can shoot bullets once per second
* player can move left or right continously to avoid bullets shot by enemies and to aim for enemies
* enemy bullet does not affect other enemy
* enemy bullet destroys players spaceship
* player bullet destroys enemy spaceship
* ESC button moves to main menu

**Win-loose conditions**
* all enemies are destroyed - win
* player spaceship is destroyed - loose
* any enemy reaching player level - loose

After win or loose game is paused and gameplay outcome(win/loose) is shown with "return to main" button (which launches main menu loading).

**Main menu**
Main menu consists buttons:
* start game (launches gameplay loading)
* quit (closes game)

**Loading screen**
* informs that next game state is loading

using UnityEngine;
using Game.Interfaces;
using Game.Implementation;
using Game.GameEnginePorts;
using EngineAdapters;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Main
{   
    [CreateAssetMenu(fileName = "Main", menuName = "SpaceInvaders/Main", order = 0)]
    public class Main : ScriptableObject 
    {
        private const string RESOURCES_FOR_LAUNCH_PATH = "Main";
        private const string RESOURCE_FOR_LAUNCH_NAME = "main";
        private const string RESOURCE_FOR_LAUNCH_NAME_EXTENSION = "asset";
        private const string RESOURCES_PATH = "Assets/Resources";

        [SerializeField] private SerializableScenesConfiguration scenesConfiguration;
        [SerializeField] private bool isStartupFileOrigin = false;

        private static Main resourcesStartupInstance;

#if UNITY_EDITOR
        private static Main startupOrigin;

        private void OnEnable() {
            OnValidate();
        }

        private void OnValidate() {
            if(isStartupFileOrigin && startupOrigin != this)
            {
                SetAsStartupInstanceOrigin();
            }
            else if(isStartupFileOrigin)
            {
                startupOrigin = this;
                UpdateStartupInstance();
            }
            else if(startupOrigin != this || resourcesStartupInstance == this || this == FetchStartupInstanceFromResources())
            {
                isStartupFileOrigin = false;
            }
            else if(startupOrigin == this)
            {
                isStartupFileOrigin = true;
            }
        }

        [ContextMenu("Set as startup instance")]
        public void SetAsStartupInstanceOrigin()
        {
            if(this == resourcesStartupInstance || this == FetchStartupInstanceFromResources())
            {
                this.isStartupFileOrigin = false;
                return;
            }
            if(startupOrigin != null)
            {
                startupOrigin.isStartupFileOrigin = false;
            }
            startupOrigin = this;
            UpdateStartupInstance();
        }

        public void UpdateStartupInstance()
        {
            Main mainAsset = FetchStartupInstanceFromResources();
            if (mainAsset == null)
            {
                if (!AssetDatabase.IsValidFolder(RESOURCES_PATH))
                {
                    AssetDatabase.CreateFolder("Assets", "Resources");
                }
                if(!AssetDatabase.IsValidFolder(string.Format("{0}/{1}", RESOURCES_PATH, RESOURCES_FOR_LAUNCH_PATH)))
                {
                    AssetDatabase.CreateFolder(RESOURCES_PATH, RESOURCES_FOR_LAUNCH_PATH);
                }
                AssetDatabase.CreateAsset(new Main(), string.Format("{0}/{1}/{2}.{3}", RESOURCES_PATH, RESOURCES_FOR_LAUNCH_PATH, RESOURCE_FOR_LAUNCH_NAME, RESOURCE_FOR_LAUNCH_NAME_EXTENSION));
                mainAsset = FetchStartupInstanceFromResources();
            }
            CopyObject(startupOrigin, target: mainAsset);
            ReassignBuildScenes();
        }

        private void CopyObject(Main origin, Main target)
        {
            target.scenesConfiguration = origin.scenesConfiguration;
        }

        private void ReassignBuildScenes()
        {
            List<string> scenesGUIDs = new List<string>(scenesConfiguration.ScenesCount);
            for (int i = 0; i < scenesConfiguration.ScenesCount; i++)
            {
                string search = string.Format("{0} {1} t:{2}", 
                    scenesConfiguration.SceneNames[i], 
                    scenesConfiguration.ScenesLabel.Length > 0 ? scenesConfiguration.ScenesLabel : "", 
                    typeof(SceneAsset).Name);
                string[] foundGUIDs = AssetDatabase.FindAssets(search);
                if(foundGUIDs.Length == 0)
                {
                    Debug.LogError(string.Format("Scene {0} not found! Check spelling in configuration!", scenesConfiguration.SceneNames[i]));
                }
                else if(foundGUIDs.Length > 1)
                {
                    Debug.LogError(string.Format("Scene {0} not found! Multiple objects found!", scenesConfiguration.SceneNames[i]));
                }
                else
                {
                    scenesGUIDs.Add(foundGUIDs[0]);
                }
            }
            List<EditorBuildSettingsScene> scenes = new List<EditorBuildSettingsScene>(scenesGUIDs.Count);
            foreach(string GUID in scenesGUIDs)
            {
                scenes.Add(new EditorBuildSettingsScene(AssetDatabase.GUIDToAssetPath(GUID), true));
            }
            EditorBuildSettings.scenes = scenes.ToArray();
        }
#endif

        [RuntimeInitializeOnLoadMethod]
        public static void main()
        {
            if(resourcesStartupInstance == null)
            {
                resourcesStartupInstance = FetchStartupInstanceFromResources();
            }
            GameObject gameObject = new GameObject("Updater");
            Object.DontDestroyOnLoad(gameObject);
            GameUpdater updater = gameObject.AddComponent<GameUpdater>();

            Game.GameEnginePorts.ILogger logger = new UnityDebugLogger();
            IGameController gameController = new GameController(logger);
            IGameEngine engineAdapter = new EngineAdapter();
            IGameStatesFactory gameStatesFactory = new GameStatesFactory(
                logger, 
                gameController, 
                engineAdapter, 
                resourcesStartupInstance.scenesConfiguration);

            gameController.SwitchToState(gameStatesFactory.GetInitialState());

            updater.SetGameController(gameController);
        }

        private static Main FetchStartupInstanceFromResources()
        {
            resourcesStartupInstance = Resources.Load<Main>(string.Format("{0}/{1}", RESOURCES_FOR_LAUNCH_PATH, RESOURCE_FOR_LAUNCH_NAME));
            return resourcesStartupInstance;
        }
    }
}
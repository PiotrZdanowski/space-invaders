using UnityEngine;
using Game.Implementation.Gameplay;
using System;
using UnityEngine.UI;

namespace Main
{
    public class PlayerTest : MonoBehaviour, IPlayer
    {
        [SerializeField] private Button[] buttons;
        [SerializeField] private Text[] texts;

        private Action[] buttonsActions;

        public void SetTestButtonsActions(params Action[] actions)
        {
            buttonsActions = actions;
            int i = 0;
            for (; i < actions.Length && i < texts.Length; i++)
            {
                texts[i].text = actions[i].Method.Name;
            }
            for(; i< texts.Length;i++)
            {
                texts[i].text = "";
            }
        }

        public void OnButtonPressed(int i)
        {
            if(buttonsActions != null && i < buttonsActions.Length)
            {
                buttonsActions[i].Invoke();
            }
        }
    }
}
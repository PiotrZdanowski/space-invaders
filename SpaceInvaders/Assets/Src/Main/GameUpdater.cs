using UnityEngine;
using Game.Interfaces;

namespace Main
{
    internal class GameUpdater : MonoBehaviour
    {
        private IGameController gameController;

        public void SetGameController(IGameController gameController)
        {
            this.gameController = gameController;
        }

        private void Update() {
            gameController?.Update();
        }

        private void OnDestroy() {
            gameController.Dispose();
        }
    }
}
using UnityEngine;

namespace EngineAdapters
{
    public class UnityDebugLogger : Game.GameEnginePorts.ILogger
    {
        public void Log(string s)
        {
            Debug.Log(s);
        }
    }
}
using Game.GameEnginePorts;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;

namespace EngineAdapters
{
    public class EngineAdapter : IGameEngine
    {
        public event Action<string> OnSceneLoaded;
        private string currentLoadingSceneId = null;
        private Dictionary<string, ISceneReferencesComponent> sceneReferencesComponents = new Dictionary<string, ISceneReferencesComponent>();

        public void LoadScene(string sceneId, bool async = true, bool unloadOtherScenes = false)
        {
            LoadSceneMode mode = unloadOtherScenes ? LoadSceneMode.Single : LoadSceneMode.Additive;
            if(unloadOtherScenes)
            {
                sceneReferencesComponents.Clear();
            }
            if(async)
            {
                SceneManager.LoadSceneAsync(sceneId, mode).completed += OnSceneLoadingCompletd;
                currentLoadingSceneId = sceneId;
            }
            else
            {
                SceneManager.LoadScene(sceneId, mode);
                OnSceneLoaded?.Invoke(sceneId);
            }
        }

        private void OnSceneLoadingCompletd(AsyncOperation asyncOperation)
        {
            asyncOperation.completed -= OnSceneLoadingCompletd;
            OnSceneLoadingCompletd();
        }
        
        private void OnSceneLoadingCompletd()
        {
            foreach(GameObject obj in SceneManager.GetSceneByName(currentLoadingSceneId).GetRootGameObjects())
            {
                ISceneReferencesComponent sceneReferences = obj.GetComponent<ISceneReferencesComponent>();
                if(sceneReferences != null)
                {
                    UnityEngine.Debug.Log(string.Format("Adding scene reference component: {0}:{1}", sceneReferences.ToString() ,sceneReferences.referencesType.Name));
                    sceneReferencesComponents.Add(currentLoadingSceneId, sceneReferences);
                }
            }
            OnSceneLoaded?.Invoke(currentLoadingSceneId);
            currentLoadingSceneId = null;
        }

        public void UnloadScene(string sceneId, bool async = true)
        {
            OnSceneUnloadBegin(sceneId);
            SceneManager.UnloadSceneAsync(sceneId);
        }

        private void OnSceneUnloadBegin(string sceneId)
        {
            sceneReferencesComponents.Remove(sceneId);
        }

        public void Quit()
        {
            Application.Quit();
        }

        public T GetSceneReferences<T>()
        {
            foreach(ISceneReferencesComponent referencesComponent in sceneReferencesComponents.Values)
            {
                if(referencesComponent.referencesType == typeof(T))
                {
                    return referencesComponent.GetSceneReferencesAs<T>();
                }
            }

            throw new ArgumentException(string.Format("Scene Reference of type {0}, a scene is not loaded?", typeof(T).Name));
        }
    }
}
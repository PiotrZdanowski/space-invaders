using Game.GameEnginePorts;
using System;
using UnityEngine;

namespace EngineAdapters
{
    [Serializable]
    public class SerializableScenesConfiguration
    {
        [SerializeField] private string mainMenuSceneId;
        [SerializeField] private string gameplaySceneId;
        [SerializeField] private string loadingSceneId;
        [SerializeField] private string scenesLabel;

        public int ScenesCount => 3;

        public string[] SceneNames => new string[]{mainMenuSceneId, gameplaySceneId, loadingSceneId};

        public string ScenesLabel => scenesLabel;

        public static implicit operator ScenesConfiguration(SerializableScenesConfiguration serializable) => new ScenesConfiguration(mainSceneName: serializable.mainMenuSceneId, gameplaySceneName: serializable.gameplaySceneId, loadingSceneName: serializable.loadingSceneId);
    }
}
using UnityEngine;
using Game.Implementation.SceneReferences;
using Game.Implementation.Gameplay;
using System;

namespace EngineAdapters.SceneReferences
{
    internal class MainMenuSceneReferencesComponent : MonoBehaviour, ISceneReferencesComponent
    {
        [SerializeField] private GameObject playerGO;

        private IPlayer player;

        private IPlayer Player 
        {
            get
            {
                if(player == null)
                {
                    player = playerGO.GetComponent<IPlayer>();
                }
                return player;
            }
        }

        public Type referencesType => typeof(MainMenuSceneReferences);

        public T GetSceneReferencesAs<T>()
        {
            if(typeof(T) == typeof(MainMenuSceneReferences))
            {
                return (T)(object) new MainMenuSceneReferences(Player);
            }
            throw new ArgumentException(string.Format("Scene reference {0} not available!", typeof(T).Name));
        }
    }
}
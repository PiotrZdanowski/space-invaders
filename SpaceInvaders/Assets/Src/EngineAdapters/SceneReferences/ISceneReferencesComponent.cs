using System;

namespace EngineAdapters
{
    internal interface ISceneReferencesComponent{
        T GetSceneReferencesAs<T>();
        Type referencesType {get;}
    }
}
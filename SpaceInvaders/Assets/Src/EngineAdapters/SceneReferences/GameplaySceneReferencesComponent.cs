using UnityEngine;
using Game.Implementation.SceneReferences;
using Game.Implementation.Gameplay;
using System;

namespace EngineAdapters.SceneReferences
{
    internal class GameplaySceneReferencesComponent : MonoBehaviour, ISceneReferencesComponent
    {
        [SerializeField] private GameObject playerGO;

        private IPlayer player;

        private IPlayer Player 
        {
            get
            {
                if(player == null)
                {
                    player = playerGO.GetComponent<IPlayer>();
                }
                return player;
            }
        }

        public Type referencesType => typeof(GameplaySceneReferences);

        public T GetSceneReferencesAs<T>()
        {
            if(typeof(T) == typeof(GameplaySceneReferences))
            {
                return (T)(object) new GameplaySceneReferences(Player);
            }
            throw new ArgumentException(string.Format("Scene reference {0} not available!", typeof(T).Name));
        }
    }
}
namespace Util
{
    public interface IUpdatable {
        void Update();
    }
}
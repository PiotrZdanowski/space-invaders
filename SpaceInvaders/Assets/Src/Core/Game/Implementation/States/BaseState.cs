using Game.GameEnginePorts;
using Game.Interfaces;

namespace Game.Implementation.States
{
    internal abstract class BaseState : IGameState 
    {
        protected IGameController gameController;
        protected IGameEngine gameEngine;
        protected IGameStatesFactory gameStatesFactory;
        protected ILogger logger;

        protected bool disposedValue {get; private set;} = false;

        protected BaseState(IGameController gameController, IGameEngine gameEngine, IGameStatesFactory gameStatesFactory, ILogger logger)
        {
            this.gameController = gameController;
            this.gameEngine = gameEngine;
            this.gameStatesFactory = gameStatesFactory;
            this.logger = logger;
        }

        public abstract void Initialize();
        public abstract void Update();
        public virtual void Deinit()
        {
            Dispose(false);
        }

        ~BaseState()
        {
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            System.GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }
                gameEngine = null;
                gameController = null;
                gameStatesFactory = null;
                logger = null;
                OnDispose(disposing);
                disposedValue = true;
            }
        }

        protected virtual void OnDispose(bool disposing)
        {

        }
    }
}
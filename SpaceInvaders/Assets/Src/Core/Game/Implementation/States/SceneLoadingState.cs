using Game.GameEnginePorts;
using Game.Interfaces;
using System;

namespace Game.Implementation.States
{
    internal class SceneLoadingState<T> : BaseState, ILoadingState<T> where T : IGameState
    {
        ScenesConfiguration scenesConfiguration;
        public T NextState {get; private set;}
        private string sceneToLoadId;

        public SceneLoadingState(string sceneToLoadId, T nextState, IGameController gameController, IGameEngine gameEngine, IGameStatesFactory gameStatesFactory, ScenesConfiguration scenesConfiguration, ILogger logger) : base(gameController, gameEngine, gameStatesFactory, logger)
        {
            this.NextState = nextState;
            this.scenesConfiguration = scenesConfiguration;
            this.sceneToLoadId = sceneToLoadId;
        }

        public override void Initialize()
        {
            gameEngine.LoadScene(scenesConfiguration.loadingSceneName, async: false, unloadOtherScenes: true);
            gameEngine.OnSceneLoaded += OnSceneLoaded;
            logger.Log(string.Format("Initialize scene loading: {0}", sceneToLoadId));
            gameEngine.LoadScene(sceneToLoadId);
        }

        private void OnSceneLoaded(string sceneId)
        {
            gameEngine.UnloadScene(scenesConfiguration.loadingSceneName);
            logger.Log(string.Format("Scene loaded: {0}", sceneId));
            gameEngine.OnSceneLoaded -= OnSceneLoaded;
            gameController.SwitchToState(NextState);
        }

        public override void Update()
        {
            
        }

        protected override void OnDispose(bool disposing)
        {
            NextState = default(T);
        }
    }
}
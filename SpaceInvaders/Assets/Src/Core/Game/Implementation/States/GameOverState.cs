using Game.GameEnginePorts;
using Game.Interfaces;
using Game.Implementation.SceneReferences;
using Game.Implementation.Gameplay;

namespace Game.Implementation.States
{
    internal class GameOverState : BaseState
    {
        public GameOverState(IGameController gameController, IGameEngine gameEngine, IGameStatesFactory gameStatesFactory, ILogger logger) : base(gameController, gameEngine, gameStatesFactory, logger)
        {
        }

        public override void Initialize()
        {
            logger.Log("Game over state initialize");
            IPlayer player = gameEngine.GetSceneReferences<GameplaySceneReferences>().player;
            player.SetTestButtonsActions(GoToMain);
        }

        public override void Update()
        {
        }

        public void GoToMain()
        {
            gameController.SwitchToState(gameStatesFactory.GetStateThroughLoading<MainMenuState>());            
        }
    }
}
using Game.Interfaces;
using Game.GameEnginePorts;
using Game.Implementation.SceneReferences;
using Game.Implementation.Gameplay;

namespace Game.Implementation.States
{
    internal class MainMenuState : BaseState
    {
        public MainMenuState(IGameController gameController, IGameEngine gameEngine, IGameStatesFactory gameStatesFactory, ILogger logger) : base(gameController, gameEngine, gameStatesFactory, logger)
        {
        }

        public override void Update()
        {
            
        }

        public void StartGame()
        {
            gameController.SwitchToState(gameStatesFactory.GetStateThroughLoading<GameplayState>());
        }

        public void QuitApplication()
        {
            gameEngine.Quit();
        }

        public override void Initialize()
        {
            logger.Log("Main menu state initialize");
            IPlayer player = gameEngine.GetSceneReferences<MainMenuSceneReferences>().player;
            player.SetTestButtonsActions(StartGame, QuitApplication);
        }
    }
}
using Game.Interfaces;
using Game.GameEnginePorts;
using Game.Implementation.Gameplay;
using Game.Implementation.SceneReferences;

namespace Game.Implementation.States
{
    internal class GameplayState : BaseState
    {

        public GameplayState(IGameController gameController, IGameEngine gameEngine, IGameStatesFactory gameStatesFactory, ILogger logger) : base(gameController, gameEngine, gameStatesFactory, logger)
        {
        }

        public override void Update()
        {

        }

        public void EndGameplay()
        {
            gameController.SwitchToState(gameStatesFactory.GetState<GameOverState>());
        }

        public void QuitToMain()
        {
            gameController.SwitchToState(gameStatesFactory.GetStateThroughLoading<MainMenuState>());
        }

        public override void Initialize()
        {
            logger.Log("Gameplay state initialize");
            IPlayer player = gameEngine.GetSceneReferences<GameplaySceneReferences>().player;
            player.SetTestButtonsActions(EndGameplay, QuitToMain);
        }
    }
}
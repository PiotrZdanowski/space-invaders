using Game.Implementation.Gameplay;

namespace Game.Implementation.SceneReferences
{
    [System.Serializable]
    public struct MainMenuSceneReferences {
        public readonly IPlayer player;

        public MainMenuSceneReferences(IPlayer player)
        {
            this.player = player;
        }
    }
}
using Game.Implementation.Gameplay;

namespace Game.Implementation.SceneReferences
{
    [System.Serializable]
    public struct GameplaySceneReferences {
        public readonly IPlayer player;

        public GameplaySceneReferences(IPlayer player)
        {
            this.player = player;
        }
    }
}
using Game.Interfaces;
using Game.Implementation.States;
using Game.GameEnginePorts;
using System;

namespace Game.Implementation
{
    public class GameStatesFactory : IGameStatesFactory
    {
        private ILogger logger;
        private IGameController gameController;
        private IGameEngine gameEngine;
        private ScenesConfiguration scenesConfiguration;

        public GameStatesFactory(ILogger logger, IGameController gameController, IGameEngine gameEngine, ScenesConfiguration scenesConfiguration)
        {
            this.logger = logger;
            this.gameController = gameController;
            this.gameEngine = gameEngine;
            this.scenesConfiguration = scenesConfiguration;
        }

        public IGameState GetInitialState()
        {
            return GetStateThroughLoading<MainMenuState>();
        }

        public T GetState<T>() where T : class, IGameState
        {
            return GetGameStateWithInfo<T>().state;
        }

        public ILoadingState<T> GetStateThroughLoading<T>() where T : class, IGameState
        {
            GameStateWithInfo<T> info = GetGameStateWithInfo<T>();
            return new SceneLoadingState<T>(
                info.stateSceneId,
                info.state,
                gameController,
                gameEngine,
                this,
                scenesConfiguration,
                logger);
        }

        private GameStateWithInfo<T> GetGameStateWithInfo<T>() where T : class, IGameState
        {
            switch(typeof(T))
            {
                case Type type when typeof(T) == typeof(MainMenuState):
                    return new GameStateWithInfo<T>(
                        new MainMenuState(gameController, gameEngine, this, logger) as T,
                        scenesConfiguration.mainSceneName
                        );
                case Type type when typeof(T) == typeof(GameplayState):
                    return new GameStateWithInfo<T>(
                        new GameplayState(gameController, gameEngine, this, logger) as T,
                        scenesConfiguration.gameplaySceneName
                        );
                case Type type when typeof(T) == typeof(GameOverState):
                    return new GameStateWithInfo<T>(
                        new GameOverState(gameController, gameEngine, this, logger) as T,
                        scenesConfiguration.gameplaySceneName
                        );
            }
            throw new ArgumentException(string.Format("Unknown or invalid state: {0}", typeof(T).Name));
        }

        private struct GameStateWithInfo<T> where T : class, IGameState
        {
            public readonly T state;
            public readonly string stateSceneId;

            public GameStateWithInfo(T state, string stateSceneId)
            {
                this.state = state;
                this.stateSceneId = stateSceneId;
            }
        }
    }
}
using System;

namespace Game.Implementation.Gameplay
{
    public interface IPlayer {
        void SetTestButtonsActions(params Action[] actions);
    }
}
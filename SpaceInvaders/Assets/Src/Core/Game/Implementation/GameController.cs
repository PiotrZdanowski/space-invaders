﻿using Game.Interfaces;
using Game.GameEnginePorts;

namespace Game.Implementation
{
    public class GameController : IGameController
    {
        private ILogger logger;
        private IGameState currentState;
        private bool disposedValue;

        public GameController(ILogger logger)
        {
            this.logger = logger;
        }

        public void SwitchToState(IGameState newState)
        {
            logger.Log(string.Format("switching to state: {0}", newState.GetType().Name));
            currentState?.Deinit();
            currentState = null;
            currentState = newState;
            currentState.Initialize();
        }

        public void Update()
        {
            currentState?.Update();
        }

        

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    currentState.Dispose();
                }
                currentState = null;
                disposedValue = true;
            }
        }

        ~GameController()
        {
            // Nie zmieniaj tego kodu. Umieść kod czyszczący w metodzie „Dispose(bool disposing)”.
            Dispose(disposing: false);
        }

        public void Dispose()
        {
            // Nie zmieniaj tego kodu. Umieść kod czyszczący w metodzie „Dispose(bool disposing)”.
            Dispose(disposing: true);
            System.GC.SuppressFinalize(this);
        }
    }
}

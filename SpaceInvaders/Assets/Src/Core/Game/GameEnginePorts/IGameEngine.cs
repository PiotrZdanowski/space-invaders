using System;

namespace Game.GameEnginePorts
{
    public interface IGameEngine 
    {
        void LoadScene(string sceneId, bool async = true, bool unloadOtherScenes = false);
        void UnloadScene(string sceneId, bool async = true);
        void Quit();
        T GetSceneReferences<T>();

        event Action<string> OnSceneLoaded;
    }
}
namespace Game.GameEnginePorts 
{
    public struct ScenesConfiguration
    {
        public readonly string mainSceneName;
        public readonly string loadingSceneName;
        public readonly string gameplaySceneName;

        public ScenesConfiguration(string mainSceneName, string loadingSceneName, string gameplaySceneName)
        {
            this.mainSceneName = mainSceneName;
            this.loadingSceneName = loadingSceneName;
            this.gameplaySceneName = gameplaySceneName;
        }
    }
}
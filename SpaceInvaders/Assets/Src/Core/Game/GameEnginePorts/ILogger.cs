namespace Game.GameEnginePorts
{
    public interface ILogger {
        void Log(string s);
    }
}
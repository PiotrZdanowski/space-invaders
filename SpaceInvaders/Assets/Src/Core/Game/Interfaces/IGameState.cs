using Util;
using System;

namespace Game.Interfaces
{
    public interface IGameState : IUpdatable, IDisposable
    {
        void Initialize();
        void Deinit();
    }
}
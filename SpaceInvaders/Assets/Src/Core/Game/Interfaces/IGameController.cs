using Util;
using System;

namespace Game.Interfaces
{
    public interface IGameController : IUpdatable, IDisposable
    {
        void SwitchToState(IGameState newState);
    }
}
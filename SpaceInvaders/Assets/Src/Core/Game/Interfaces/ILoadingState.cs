namespace Game.Interfaces
{
    public interface ILoadingState<T> : IGameState where T : IGameState
    {
        T NextState {get;}
    }
}
namespace Game.Interfaces
{
    public interface IGameStatesFactory 
    {
        T GetState<T>() where T : class, IGameState;
        ILoadingState<T> GetStateThroughLoading<T>() where T : class, IGameState;
        IGameState GetInitialState();
    }
}